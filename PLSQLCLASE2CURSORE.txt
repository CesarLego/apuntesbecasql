DECLARE
CURSOR cur_depts_emps IS
SELECT department_name, COUNT(*) AS how_many
FROM departments d, employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING COUNT(*) > 1;

*Estudiar Group BY

++++++++++++++++++++++++++++++++++++++++++++

DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name
    FROM
        departments;

    v_department_id    departments.department_id%TYPE;
    v_department_name  departments.department_name%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name;
        EXIT WHEN cur_depts%notfound;
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name);
    END LOOP;

    CLOSE cur_depts;
END;



SELECT department_name, COUNT(*) AS how_many
FROM departments d, employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING COUNT(*) > 1;


DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name,
        salary
    FROM
        employees
    WHERE
        department_id = 30;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
    v_sal    employees.salary%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname,
            v_sal;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno|| ' '|| v_lname|| ' '|| v_sal);
    END LOOP;
    CLOSE cur_emps;
END;

----DECLARACION DE UN CURSOR
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 10;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
    END LOOP;
  CLOSE cur_emps;
END;


-----------------------------Extraer nuchos campos mediante un ROWTYPE
---Este ejemplo simplifica la declaración de un cursos para un tabla que tiene muchos campos
DECLARE
    CURSOR cur_emps IS
    SELECT
        *
    FROM
        employees
    WHERE
        department_id = 30;

    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' - '
                             || v_emp_record.last_name);
    END LOOP;

    CLOSE cur_emps;
END;


-------------------------------------
--------------SE DEFINE UN CURSOR A PARTIR DE DOS TABLAS
DECLARE
    CURSOR cur_emps_dept IS
    SELECT
        first_name,
        last_name,
        department_name
    FROM
        employees    e,
        departments  d
    WHERE
        e.department_id = d.department_id;

    v_emp_dept_record cur_emps_dept%rowtype;
BEGIN
    OPEN cur_emps_dept;
    LOOP
        FETCH cur_emps_dept INTO v_emp_dept_record;
        EXIT WHEN cur_emps_dept%notfound;
        dbms_output.put_line(v_emp_dept_record.first_name
                             || ' – '
                             || v_emp_dept_record.last_name
                             || ' – '
                             || v_emp_dept_record.department_name);

    END LOOP;

    CLOSE cur_emps_dept;
END;

-------------------------------------------------------------------------

DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees;

    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%rowcount > 10 OR cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;

    CLOSE cur_emps;
END;